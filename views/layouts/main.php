<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;

AppAsset::register($this);

$home=Yii::$app->homeUrl;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' =>Html::encode(Yii::$app->name)." ".Html::tag('i',"",['class'=>"fas fa-book-reader"]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left nav-pills'],
        'items' => [
            ['label' =>'Inicio <span class="glyphicon glyphicon-home"></span>', 'url' => ['/site/index']],
            ['label' => 'Libros <i class="fas fa-book"></i>', 'url' => ['/libros/index']],
            ['label' => 'Autores <i class="fas fa-user-edit"></i>', 'url' => ['/autores/index']],
            ['label' => 'Editoriales <i class="fas fa-landmark"></i>', 'url' => ['/editoriales/index']],
            ['label' => 'Temáticas <i class="fas fa-theater-masks"></i>',
                'items'=>[
                    ['label' => 'Temáticas generales', 'url' => ['/tematicas/index2']],
                    ['label' => 'Tématicas y subtemáticas', 'url' => ['/tematicas/index']],
                ],
            ],
        ],
        'encodeLabels' => false,
    ]);
    echo Html::beginForm(["site/busqueda"],"post",["class"=>"navbar-form navbar-right","id"=>"form1"]);
            echo '<div class="form-group">';
            echo  '<input id="texto1" type="text" class="form-control buscar" placeholder="&#xF002;" name="busqueda" onkeyup="comprueba(this)" />';
            echo  '<button type="submit" class="btn btn-default">Buscar</button>';
            echo  '</div>';
            echo Html::endForm();
    NavBar::end();
    ?>

    <div class="container principal">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">Alberto Ceballos <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script type="text/javascript">
    function comprueba(e){
        texto=$(e).val();
        //console.log(texto);
        $.ajax({
           url:'busquedaajax',
           type:'post',
           data:{texto:texto},
           success:function(data){
              datos=JSON.parse(data);
              console.log(datos);
              $("#resultados").html("");
              for(i=0;i<datos.titulo.length;i++){
              document.querySelector("#resultados").innerHTML+=('<div class="panel panel-primary"><div class="panel-heading">'+datos.titulo[i]+'</div><div class="panel-body">'+datos.autor[i]+'<br>'+datos.editorial[i]+'</div></div>');   
            }
           }
                   
        });
    }
    
    
</script>