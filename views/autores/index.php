<?php

use yii\helpers\Html;
use yii\widgets\ListView;


$model= new \app\models\Autores();

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear autor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <table class="table table-striped">
    <tr>
        <th><?=$model->getAttributeLabel('id_autor') ?></th>
        <th><?=$model->getAttributeLabel('autor') ?></th>
        <th><?=$model->getAttributeLabel('nacionalidad') ?></th>
        <th>Libros del autor</th>
    </tr>
    <?=    ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView'=>'_vistaListaAutores',
            'summary'=>'Mostrando {begin}-{end} de {totalCount} autores',
    ]); ?>
    </table>
</div>
