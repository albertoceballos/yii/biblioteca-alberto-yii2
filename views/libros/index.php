<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */


$this->title = 'Libros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
  <div class="row flex-row">
   <?php
    foreach($datos as $registro){
   ?>    
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <!--Para mostrar las imagenes guardadas en la base de datos en formato Blob -->
        <img src="<?= 'data:image/jpeg;base64,'.base64_encode($registro['foto'] )?>">
      <div class="caption">
        <h3><?= Html::a($registro["titulo"],['libros/view','id'=>$registro['id_libro']] )?></h3>
        <?php $id=$registro["id_libro"] ?>
        <p><?php $autores=Yii::$app->db->createCommand("SELECT autor FROM autores JOIN escriben USING(id_autor) WHERE id_libro=$id")->queryAll();
            foreach ($autores as $autor){
                echo $autor["autor"]."<br>";
            }
            ?></p>
        <p><?= $registro["anio"] ?> - <?= $registro["editorial"] ?></p>
        <p>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapse<?= $registro["id_libro"] ?>" aria-expanded="false" aria-controls="collapseExample">        
                Ver detalles
            </button>
            <button class="btn btn-success" type="button" data-toggle="modal" data-target="#Modal<?= $registro["id_libro"] ?>">Ver descripción</button>
        </p>  
            <div class="collapse" id="collapse<?= $registro["id_libro"] ?>">
                <div class="well flex-text">
                    <p>Temáticas:
                    <?php
                    $tematicas=Yii::$app->
                            db->
                            createCommand("SELECT tematica FROM libros JOIN reproducen USING (id_libro) JOIN tematicas USING (id_tematica) WHERE libros.id_libro=$id")
                            ->queryAll();
                    
                            foreach($tematicas as $clave=>$valor){
                                echo $valor["tematica"];
                                if($clave!=count($tematicas)-1){
                                    echo ",";
                                }
                            }
                    ?></p>
                    <p>ID: <?= $registro["id_libro"] ?></p>
                    <p>ISBN: <?= $registro["ISBN"] ?></p>
                    <p>Leído: <?php if($registro["leido"]==0){ echo "No";}else{ echo "Sí";} ?></p>
                </div>
            </div>
      </div>
    </div>
      
      <div class="modal fade" id="Modal<?= $registro["id_libro"] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel"><?= $registro["titulo"] ?></h4>
            </div>
            <div class="modal-body">
              <?php if($registro["descripcion"]!=NULL){echo $registro["descripcion"];}else{echo "No hay Descripción";} ?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
       </div>
  </div>
   <?php
    }
    ?>
</div>
    
    
</div>
