<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LibrosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="libros-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_libro') ?>

    <?= $form->field($model, 'titulo') ?>

    <?= $form->field($model, 'anio') ?>

    <?= $form->field($model, 'ISBN') ?>

    <?= $form->field($model, 'leido') ?>

    <?php // echo $form->field($model, 'id_coleccion') ?>

    <?php // echo $form->field($model, 'id_editorial') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
