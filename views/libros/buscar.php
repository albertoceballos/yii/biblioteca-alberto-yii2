<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $searchModel app\models\LibrosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

echo $this->render('_search', ['model' => $searchModel]); 
  
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary'=>'Mostrando {begin}-{end} de {totalCount} Libros',
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id_libro',
            'titulo',
            'anio',
            'ISBN',
            'leido',
            //'id_coleccion',
            //'id_editorial',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);

