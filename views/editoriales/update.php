<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Editoriales */

$this->title = 'Update Editoriales: ' . $model->id_editorial;
$this->params['breadcrumbs'][] = ['label' => 'Editoriales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_editorial, 'url' => ['view', 'id' => $model->id_editorial]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="editoriales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
