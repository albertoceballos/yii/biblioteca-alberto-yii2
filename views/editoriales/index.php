<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Editoriales;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Editoriales';
$this->params['breadcrumbs'][] = $this->title;

$model=$dataProvider->models;

?>
<div class="editoriales-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Editorial', ['create'], ['class' => 'btn btn-success']) ?>
        <?php  //echo $this->render('_search', ['model' => $searchModel]); ?> 
    </p>
   
    
    <?=    Gridview::widget([
        'dataProvider' => $dataProvider,
      
        'summary'=>'Mostrando {begin} - {end} de {totalCount} editoriales',
        'filterModel' => $searchModel,
        'columns' => [
           //['class' => 'yii\grid\SerialColumn'],    
            'id_editorial',
            'editorial',
            [
               'label'=>'Libros de la Editorial',
                'format'=>'raw',
                'value'=> function($model){
                    $libros=$model->getLibros()->all();
                    $url="";
                    foreach($libros as $reg){
                        $url.=Html::a($reg->titulo,["libros/view","id"=>$reg->id_libro])."<br>";
                    }
                    return  $url;
               }
            ],
        ]
    ]); ?>
</div>
