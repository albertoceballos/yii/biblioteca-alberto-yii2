<?php

use yii\helpers\Html;
use yii\grid\GridView;



/* @var $this yii\web\View */

$this->title = 'Tematicas y subtématicas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tematicas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Temática', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
        foreach($modelo as $valor){
    ?>
    <div class="panel panel-success">
        <div class="panel-heading "><h4><?= $valor["tematica"] ?></h4></div>
        <div class="panel-body">
           <?php  
               $tematicas=$valor->getTematicas()->asArray()->all();
               foreach($tematicas as $tema){
                   echo"<h4>". Html::a($tema['tematica'],['tematicas/muestralibros','id'=>$tema['id_tematica']])."</h4>";
                   
                   if(isset($id_pulsada) && $id_pulsada==$tema['id_tematica']){
                        if(isset($libros)){
                            foreach ($libros as $libro){
                                echo $libro->titulo."<br>";
                            }
                        }
                   }
               }
              
           ?>
        </div>
    </div>
    <?php    
        }
    ?>

</div>
