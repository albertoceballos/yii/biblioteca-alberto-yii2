<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\BaseUrl;



/* @var $this yii\web\View */

$this->title = 'Tematicas Generales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tematicas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    //var_dump($datos);
    
    foreach($datos as $val){
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading"><?= $val["tematica"] ?></div>
         <div class="panel-body">
            <?php
                $cuenta= Yii::$app->db->createCommand("SELECT COUNT(*) cuenta FROM reproducen WHERE id_tematica=".$val["id_tematica"])->queryAll();
                foreach ($cuenta as $v){
                    echo "<a onclick='funcion1(this)' href='#' id=".$val['id_tematica']." >Nº de Títulos: ". $v["cuenta"]."</a>";
                    
                }
                echo "<div style='display:none' id='div".$val["id_tematica"]."'></div>"
            ?>
        </div>
    </div>
    <?php
    }
    ?>
    <script>
    function funcion1(e)
    {
        id=$(e).attr('id');
        
        $.ajax({
           url: 'librosgenerales',
           type: 'post',
           data: {id: id},
           success: function (data) {
               $("#div"+id).html(data);
               $("#div"+id).toggle("slow");
           }

      });
    }
</script>
</div>