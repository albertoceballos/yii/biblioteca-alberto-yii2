<?php

namespace app\controllers;

use Yii;
use app\models\Tematicas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TematicasController implements the CRUD actions for Tematicas model.
 */
class TematicasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tematicas models.
     * @return mixed
     */
    public function actionIndex()
    {
       
        $modelo= Tematicas::find()->where(['id_padre'=>1])->all();
            
        return $this->render('index', [
            'modelo' => $modelo,
        ]);
    }
    
    public function actionMuestralibros($id){
        $modelo= Tematicas::find()->where(['id_padre'=>1])->all();
        $consulta= Tematicas::find()->where(['id_tematica'=>$id])->all();
        foreach($consulta as $valor){
            $libros=$valor->getLibros()->all();
        }
              
        return $this->render('index', [
            'modelo' => $modelo,'libros'=>$libros,'id_pulsada'=>$id
        ]);
        
    }
    
    public function actionIndex2(){
        $modelo= Tematicas::find()->where(['id_padre'=>1])->all();
  
        return $this->render('index2',['datos'=>$modelo]);
    }
    
    public function actionLibrosgenerales(){
        $id = $_POST['id'];
        $consulta= Tematicas::find()->where(['id_tematica'=>$id])->all();
        foreach($consulta as $reg){
            $libros=$reg->getLibros()->all();
            foreach($libros as $libro){
                echo "<br>". \yii\helpers\Html::a($libro->titulo,['libros/view','id'=>$libro->id_libro])."<br>";
            }
        }
    }

    /**
     * Displays a single Tematicas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tematicas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tematicas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tematica]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tematicas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_tematica]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tematicas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tematicas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tematicas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tematicas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
