<?php

namespace app\controllers;

use Yii;
use app\models\Libros;
use app\models\LibrosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LibrosController implements the CRUD actions for Libros model.
 */
class LibrosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Libros models.
     * @return mixed
     */
    public function actionIndex(){
        $consulta= Yii::$app->db->createCommand("SELECT DISTINCT libros.*,editorial FROM libros JOIN escriben USING (id_libro)
        JOIN autores USING (id_autor)
        JOIN editoriales USING (id_editorial);")
                ->queryAll();
        return $this->render('index',['datos'=>$consulta]);

    }
    
   
    
    
    public function actionBuscar()
    {
        $searchModel = new LibrosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Libros model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $leido=($model->leido==0)?"No":"Si";
        $coleccion=($model->id_coleccion==null)?"No tiene Colección":$model->id_coleccion;
        $autores= Yii::$app->db->createCommand("SELECT autor from autores JOIN escriben USING(id_autor) WHERE id_libro=$model->id_libro")->queryAll();
        $autor="";
        foreach($autores as $valor){
            $autor.= $valor["autor"]."  ";
        }
        $editorial= (new \yii\db\Query())
                ->select('editorial')
                ->from('editoriales')
                ->join('INNER JOIN', 'libros', 'editoriales.id_editorial=libros.id_editorial')
                ->where("id_libro=$id")
                ->one();
        
        return $this->render('view', [
            'model' =>$model,
            'leido'=>$leido,
            'coleccion'=>$coleccion,
            'autor'=>$autor,
            'editorial'=>$editorial["editorial"],
        ]);
    }

    /**
     * Creates a new Libros model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Libros();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_libro]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Libros model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_libro]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Libros model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Libros model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Libros the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Libros::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
