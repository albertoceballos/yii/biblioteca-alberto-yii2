<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    
    public function actionIndex()
    {
        return $this->render('index');
    }

   
    public function actionBusqueda(){
        //if($_POST["busqueda"]!=""){
        $cadena=$_POST["busqueda"];
       
  
        $datos= \Yii::$app->db->createCommand(
            'SELECT * FROM libros JOIN escriben USING (id_libro)
            JOIN autores USING (id_autor)
            JOIN editoriales USING (id_editorial)
            JOIN reproducen USING (id_libro)
            JOIN tematicas USING (id_tematica)
            WHERE titulo LIKE "%'.$cadena.'%" or tematica LIKE "%'.$cadena.'%" OR autor LIKE "%'.$cadena.'%" OR editorial LIKE "%'.$cadena.'%"
            GROUP BY libros.id_libro')->queryAll();
    
        return $this->render('busqueda',['datos'=>$datos]);
        //}else{
          //return $this->render('error',['name'=>"Error",'message'=>'No has introducido ningún patrón de búsqueda']);
       // }
    }
    
    public function actionBusquedaajax(){
        
        $cadena=\Yii::$app->request->post("texto");
        $consulta= \Yii::$app->db->createCommand('SELECT * FROM libros JOIN escriben USING (id_libro)
            JOIN autores USING (id_autor)
            JOIN editoriales USING (id_editorial)
            JOIN reproducen USING (id_libro)
            JOIN tematicas USING (id_tematica)
            WHERE titulo LIKE "%'.$cadena.'%" or tematica LIKE "%'.$cadena.'%" OR autor LIKE "%'.$cadena.'%" OR editorial LIKE "%'.$cadena.'%"
            GROUP BY libros.id_libro')->queryAll();
            
            $obj=["titulo"=>[],
                   "autor"=>[],
                   "editorial"=>[],
                ];        
            foreach($consulta as $dato){
                $obj["titulo"][]=$dato["titulo"];
                $obj["autor"][]=$dato["autor"];
                $obj["editorial"][]=$dato["editorial"];
            }
            echo json_encode($obj);
        
        
        //return $this->renderAjax('busqueda');
    
    }
    
    
    
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

  
}
