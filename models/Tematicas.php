<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tematicas".
 *
 * @property int $id_tematica
 * @property string $tematica
 * @property int $id_padre
 *
 * @property Reproducen[] $reproducens
 * @property Libros[] $libros
 * @property Tematicas $padre
 * @property Tematicas[] $tematicas
 */
class Tematicas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tematicas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_padre'], 'integer'],
            [['tematica'], 'string', 'max' => 255],
            [['id_padre'], 'exist', 'skipOnError' => true, 'targetClass' => Tematicas::className(), 'targetAttribute' => ['id_padre' => 'id_tematica']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tematica' => 'Id Tematica',
            'tematica' => 'Tematica',
            'id_padre' => 'Id Padre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReproducens()
    {
        return $this->hasMany(Reproducen::className(), ['id_tematica' => 'id_tematica']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['id_libro' => 'id_libro'])->viaTable('reproducen', ['id_tematica' => 'id_tematica']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPadre()
    {
        return $this->hasOne(Tematicas::className(), ['id_tematica' => 'id_padre']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTematicas()
    {
        return $this->hasMany(Tematicas::className(), ['id_padre' => 'id_tematica']);
    }
}
