<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "editoriales".
 *
 * @property int $id_editorial
 * @property string $editorial
 *
 * @property Libros[] $libros
 */
class Editoriales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'editoriales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['editorial'], 'required'],
            [['editorial'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_editorial' => 'Id Editorial',
            'editorial' => 'Editorial',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['id_editorial' => 'id_editorial']);
    }
}
