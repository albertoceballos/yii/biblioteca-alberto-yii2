<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id_libro
 * @property string $titulo
 * @property string $anio
 * @property string $ISBN
 * @property int $leido
 * @property int $id_coleccion
 * @property int $id_editorial
 * @property string $descripcion
 * @property resource $foto
 *
 * @property Escriben[] $escribens
 * @property Autores[] $autors
 * @property Editoriales $editorial
 * @property Colecciones $coleccion
 * @property Reproducen[] $reproducens
 * @property Tematicas[] $tematicas
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['leido', 'id_coleccion', 'id_editorial'], 'integer'],
            [['descripcion', 'foto'], 'string'],
            [['titulo', 'ISBN'], 'string', 'max' => 255],
            [['anio'], 'string', 'max' => 4],
            [['id_editorial'], 'exist', 'skipOnError' => true, 'targetClass' => Editoriales::className(), 'targetAttribute' => ['id_editorial' => 'id_editorial']],
            [['id_coleccion'], 'exist', 'skipOnError' => true, 'targetClass' => Colecciones::className(), 'targetAttribute' => ['id_coleccion' => 'id_coleccion']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_libro' => 'Id Libro',
            'titulo' => 'Titulo',
            'anio' => 'Año',
            'ISBN' => 'ISBN',
            'leido' => 'Leido',
            'id_coleccion' => 'Id Coleccion',
            'id_editorial' => 'Id Editorial',
            'descripcion' => 'Descripcion',
            'foto' => 'Foto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEscribens()
    {
        return $this->hasMany(Escriben::className(), ['id_libro' => 'id_libro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutors()
    {
        return $this->hasMany(Autores::className(), ['id_autor' => 'id_autor'])->viaTable('escriben', ['id_libro' => 'id_libro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEditorial()
    {
        return $this->hasOne(Editoriales::className(), ['id_editorial' => 'id_editorial']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColeccion()
    {
        return $this->hasOne(Colecciones::className(), ['id_coleccion' => 'id_coleccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReproducens()
    {
        return $this->hasMany(Reproducen::className(), ['id_libro' => 'id_libro']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTematicas()
    {
        return $this->hasMany(Tematicas::className(), ['id_tematica' => 'id_tematica'])->viaTable('reproducen', ['id_libro' => 'id_libro']);
    }
}
