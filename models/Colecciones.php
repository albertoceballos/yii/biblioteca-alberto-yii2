<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "colecciones".
 *
 * @property int $id_coleccion
 * @property string $coleccion
 * @property int $capitulos
 * @property int $leidos
 *
 * @property Libros[] $libros
 */
class Colecciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'colecciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['capitulos', 'leidos'], 'integer'],
            [['coleccion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_coleccion' => 'Id Coleccion',
            'coleccion' => 'Coleccion',
            'capitulos' => 'Capitulos',
            'leidos' => 'Leidos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['id_coleccion' => 'id_coleccion']);
    }
}
