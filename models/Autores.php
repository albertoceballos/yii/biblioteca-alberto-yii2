<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autores".
 *
 * @property int $id_autor
 * @property string $autor
 * @property string $nacionalidad
 *
 * @property Escriben[] $escribens
 * @property Libros[] $libros
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autor'], 'required'],
            [['autor', 'nacionalidad'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_autor' => 'Id Autor',
            'autor' => 'Autor',
            'nacionalidad' => 'Nacionalidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEscribens()
    {
        return $this->hasMany(Escriben::className(), ['id_autor' => 'id_autor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['id_libro' => 'id_libro'])->viaTable('escriben', ['id_autor' => 'id_autor']);
    }
}
